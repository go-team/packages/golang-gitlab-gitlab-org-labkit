//go:build fips
// +build fips

package fips

import (
	"io"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestEnabled(t *testing.T) {
	file, err := os.Open("/proc/sys/crypto/fips_enabled")

	if err != nil {
		require.False(t, Enabled())
		require.False(t, Check())
		return
	}

	defer file.Close()

	b, err := io.ReadAll(file)

	if err != nil {
		require.False(t, Enabled())
		require.False(t, Check())
	} else {
		require.Equal(t, strings.TrimSpace(string(b)) == "1", Enabled())
		require.Equal(t, strings.TrimSpace(string(b)) == "1", Check())
	}
}
