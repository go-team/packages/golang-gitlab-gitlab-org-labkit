// +build tracer_static,tracer_static_jaeger

package impl

import (
	"reflect"
	"strings"
	"testing"

	"go.opencensus.io/trace"
)

type unsupportedType struct{}

func TestOcSpanAdapterCastToAttribute(t *testing.T) {
	tests := []struct {
		name  string
		key   string
		value interface{}
		want  []trace.Attribute
	}{
		{
			name:  "true",
			key:   "foo",
			value: true,
			want:  []trace.Attribute{trace.BoolAttribute("foo", true)},
		},
		{
			name:  "false",
			key:   "foo",
			value: false,
			want:  []trace.Attribute{trace.BoolAttribute("foo", false)},
		},
		{
			name:  "0",
			key:   "foo",
			value: 0,
			want:  []trace.Attribute{trace.Int64Attribute("foo", 0)},
		},
		{
			name:  "42",
			key:   "foo",
			value: 42,
			want:  []trace.Attribute{trace.Int64Attribute("foo", 42)},
		},
		{
			name:  "42.1",
			key:   "foo",
			value: 42.1,
			want:  []trace.Attribute{trace.Float64Attribute("foo", 42.1)},
		},
		{
			name:  "short string",
			key:   "foo",
			value: "bar",
			want:  []trace.Attribute{trace.StringAttribute("foo", "bar")},
		},
		{
			name:  "empty string",
			key:   "foo",
			value: "",
			want:  []trace.Attribute{trace.StringAttribute("foo", "")},
		},
		{
			name:  "string length 255",
			key:   "foo",
			value: strings.Repeat("a", 255),
			want:  []trace.Attribute{trace.StringAttribute("foo", strings.Repeat("a", 255))},
		},
		{
			name:  "string length 256",
			key:   "foo",
			value: strings.Repeat("a", 256),
			want:  []trace.Attribute{trace.StringAttribute("foo", strings.Repeat("a", 256))},
		},
		{
			name:  "string length 257",
			key:   "foo",
			value: strings.Repeat("a", 257),
			want: []trace.Attribute{
				trace.StringAttribute("foo", strings.Repeat("a", 256)),
				trace.StringAttribute("foo.1", strings.Repeat("a", 1)),
			},
		},
		{
			name:  "string length 511",
			key:   "foo",
			value: strings.Repeat("a", 511),
			want: []trace.Attribute{
				trace.StringAttribute("foo", strings.Repeat("a", 256)),
				trace.StringAttribute("foo.1", strings.Repeat("a", 255)),
			},
		},
		{
			name:  "string length 512",
			key:   "foo",
			value: strings.Repeat("a", 512),
			want: []trace.Attribute{
				trace.StringAttribute("foo", strings.Repeat("a", 256)),
				trace.StringAttribute("foo.1", strings.Repeat("a", 256)),
			},
		},
		{
			name:  "string length 513",
			key:   "foo",
			value: strings.Repeat("a", 513),
			want: []trace.Attribute{
				trace.StringAttribute("foo", strings.Repeat("a", 256)),
				trace.StringAttribute("foo.1", strings.Repeat("a", 256)),
				trace.StringAttribute("foo.2", strings.Repeat("a", 1)),
			},
		},
		{
			name:  "unsupported type",
			key:   "foo",
			value: unsupportedType{},
			want: []trace.Attribute{
				trace.StringAttribute("foo", "castToAttribute not implemented for type struct"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := castToAttribute(tt.key, tt.value) //nolint

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("castToAttribute() got = %v, want %v", got, tt.want)
			}
		})
	}
}
